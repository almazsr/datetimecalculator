﻿using System;

namespace DateTimeCalculator
{
    public interface ICurrentDateTimeProvider
    {
        DateTimeOffset Get();
    }

    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTimeOffset Get() => DateTimeOffset.Now;
    }

    public interface IDateTimeCalculator
    {
        string Calculate(string expression);
    }

    public interface IDateTimeParser
    {

    }

    public class DateTimeCalculator : IDateTimeCalculator
    {
        public string Calculate(string expression)
        {

        }
    }
}
